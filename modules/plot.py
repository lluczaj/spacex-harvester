'''Module for plot generation from dataframes'''
import matplotlib.pyplot as plt

def generate_plots(pd_yearly, df_landpads):
    '''Generates various plots from provided dataframes'''

    #Basic plot of attmpted flights vs successful launches per year
    pd_yearly.plot(y=[
        "attempted_flights",
        "succesful_launches",
        ], x="launch_year")
    plt.title("Year by year launches")
    plt.savefig("plots/yearly_launches.png")

    #Basic plot of launches success rate per year
    pd_yearly.plot(y="success_rate", x="launch_year")
    plt.title("Year by year launch success rate")
    plt.savefig("plots/yearly_success_rate.png")

    #Bar plot of attempted vs successful landings per landpad
    df_landpads.plot.bar(y=[
        "attempted_landings",
        "successful_landings"
        ], x="id")
    plt.title("Landpads attempted vs successful landings")
    plt.savefig("plots/landingpads.png")

    #Bar plot of overall and failed launches cost per year
    pd_yearly.plot.bar(y=[
        "yearly_cost",
        "failed_launches_cost"
        ], x="launch_year")
    plt.title("Year by year overall and failed launches cost [x 1 000 000 $]")
    plt.savefig("plots/costs.png")