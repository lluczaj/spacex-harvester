import sys

def generate_csv(df, name, logger):
    '''Module for generation of CSV files'''
    logger.debug("Generating %s CSV file", name)
    try:
        df.to_csv("csv/" + name + ".csv")
    except FileNotFoundError as error:
        logger.error(error)
        sys.exit(1)

    logger.info("%s CSV file generated succesfully", name)
