'''SpaceX data harvester'''
import sys
import os
import logging
import requests
import pandas as pd
from modules.parsers import (
    launches_parser,
    rocket_parser,
    yearly_parser
    )
from modules.plot import generate_plots
from modules.csv import generate_csv

#Generate new logger, set formatting and handlers (console + file)
LOGGER = logging.getLogger('spacex_logger')
LOGGER.setLevel(logging.os.environ.get("LOGLEVEL", "INFO"))
CH = logging.StreamHandler()
CH.setLevel(logging.DEBUG)
FORMATTER = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
CH.setFormatter(FORMATTER)
LOGGER.addHandler(CH)
FH = logging.FileHandler(r'log/spacex.log')
FH.setFormatter(FORMATTER)
LOGGER.addHandler(FH)

#Access SpaceX API endpoints and collect specific metrics data as json
LOGGER.debug("Accessing spaceX API")
try:
    LAUNCHES = requests.get(
        'https://api.spacexdata.com/v3/launches/past?filter=flight_number,mission_name,launch_year,launch_success,rocket/rocket_id'
        ).json()
    ROCKETS = requests.get('https://api.spacexdata.com/v3/rockets?filter=active,cost_per_launch,rocket_id').json()
    LANDPADS = requests.get('https://api.spacexdata.com/v3/landpads?filter=id,status,attempted_landings,successful_landings').json()
except requests.exceptions.RequestException as error:
    LOGGER.error(error)
    sys.exit(1)

LOGGER.info("SpaceX API data collected succesfully")

#Create Pandas dataframes from collected SpaceX data
DF_LAUNCHES = pd.DataFrame(LAUNCHES)
DF_ROCKETS = pd.DataFrame(ROCKETS)
DF_LANDPADS = pd.DataFrame(LANDPADS)
LAUNCH_PARSE = launches_parser(DF_LAUNCHES, DF_ROCKETS)
DF_LAUNCHES = LAUNCH_PARSE[1]
DF_YEARLY = pd.DataFrame(yearly_parser(DF_LAUNCHES))
ROCKETS = rocket_parser(DF_ROCKETS)

#Generate CSV files and plots
generate_csv(DF_LAUNCHES, "all_launches", LOGGER)
generate_csv(DF_YEARLY, "yearly_launches", LOGGER)
generate_csv(DF_LANDPADS, "landpads", LOGGER)
generate_plots(DF_YEARLY, DF_LANDPADS)

LOGGER.info('Data collected succesfully, check CSV directory for detailed flights information')
#Print some basic information
print('Total number of attempted flights is', DF_LAUNCHES.shape[0])
print('Current flight success rate is', LAUNCH_PARSE[0])
print('Number of active rockets:', ROCKETS)
