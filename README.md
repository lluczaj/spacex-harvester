# **Simple SpaceX data harvester**

This is a simple demo program for collecting and parsing data from SpaceX REST API endpoint.


## Requirements

 - Python3
 - Required python3 modules are listed in *requirements.txt* file. You can run the following command to install needed packages:
  `$ pip install -r requirements.txt`

## Basic usage
You can run this script by typing the following command in your shell:
`$ python3 spacex.py`

## Logging
Default logging level is set to INFO. You can change logging level by setting an environment variable LOGLEVEL to *DEBUG* or *ERROR*.
`export LOGLEVEL=DEBUG|ERROR`

## Results
This script generates results into the following directories:

| Directory |Description  |
|--|--|
|csv  |CSV files with overall and year-by-year statistics  |
|plots|Various Graphs/Charts/Plots based on collected data|
|logs|Logs from application run|


**Author**: Lukasz Luczaj (luczaj.lukasz@gmail.com)

