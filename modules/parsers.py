'''Set of parsing functions'''
import pandas as pd

def launches_parser(df_launches, df_rockets):
    '''Parsing overall success rate'''
    launch_success_rate = df_launches.launch_success.value_counts().loc[True]/df_launches.shape[0]
    cost = pd.Series([])
    rocket_id = pd.Series([])

    #Collect values from {"rocket": "rocket_id"} dict into new series and update df_launches dataframe with new values 
    for i in df_launches.index:
        rocket_id[i] = df_launches.rocket[i]["rocket_id"]

    df_launches.rocket.update(rocket_id)

    #Update df_launches dataframe with new column 'cost'
    for i in df_launches.index:
        cost[i] = df_rockets.cost_per_launch[df_rockets.rocket_id == df_launches.rocket[i]].values
        cost[i] = cost[i][0]
    df_launches['cost'] = cost

    return launch_success_rate, df_launches

def rocket_parser(df_rockets):
    '''Parsing rocket information'''
    active_rockets = df_rockets.active.value_counts().loc[True]
    return active_rockets

def yearly_parser(df_launches):
    '''Parsing year-by-year information'''
    uniq = df_launches.launch_year.unique()
    yearly = []
    for i in uniq:
        attempts = df_launches.launch_year.value_counts().loc[i]
        success = df_launches.launch_success[(df_launches.launch_year == i) &
                                             (df_launches.launch_success)].sum()
        cost = df_launches.cost[df_launches.launch_year == i].sum()
        launch_failed_cost = df_launches.cost[(df_launches.launch_year == i) &
                                              (df_launches.launch_success == False)].sum()
        launch_params = {
            "launch_year": i,
            "attempted_flights": attempts,
            "succesful_launches": success,
            "success_rate": success/attempts,
            "yearly_cost": cost/1000000,
            "failed_launches_cost": launch_failed_cost/1000000
            }
        yearly.append(launch_params)
    return yearly
